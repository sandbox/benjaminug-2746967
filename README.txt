Square Payments
=======================

Provides Square (https://squareup.com) payment method for Drupal.

Installation
------------

1. Install the module and its dependencies as usual. Because this module depends on Composer Manager, enabling it with Drush (drush en squareup_payments) is recommended, as Drush will automatically generate the consolidated composer.json file and run the appropriate Composer commands to install and update the required dependencies (see https://www.drupal.org/node/2405805)

2. Select a form to handle payments; to this form, add a field to hold the payment amount

3. Configure the module settings at admin/config/squareup_payment/settings

4. Configure the location settings at admin/config/squareup_payment/location

5. Create new payment instances
