<?php
/**
 * @file
 * Implements SquareUp Payments. Also implements the basic functionality
 * required to create and display squareup_payment entities.
 * 
 * @see https://docs.connect.squareup.com/articles/ecommerce-overview/.
 */

/**
 * Implements hook_entity_info().
 *
 * This is the fundamental description of the entity.
 *
 * It provides a single entity without revision support.
 */
function squareup_payment_entity_info() {
  $info['squareup_payment'] = array(
    // A human readable label to identify our entity.
    'label' => t('SquareUp Payment'),

    // The controller for our Entity, extending the Drupal core controller.
    'controller class' => 'SquareUpPaymentController',

    // The table for this entity defined in hook_schema()
    'base table' => 'squareup_payment',

    // Returns the uri elements of an entity
    'uri callback' => 'squareup_payment_uri',

    // IF fieldable == FALSE, we can't attach fields.
    'fieldable' => FALSE,

    // entity_keys tells the controller what database fields are used for key
    // functions. It is not required if we don't have bundles or revisions.
    // Here we do not support a revision, so that entity key is omitted.
    'entity keys' => array(
      'id' => 'instance_id' , // The 'id' (instance_id here) is the unique id.
      // 'bundle' => 'bundle_type' // Bundle will be determined by the 'bundle_type' field
    ),
    // 'bundle keys' => array(
    //   'bundle' => 'bundle_type',
    // ),

    // FALSE disables caching. Caching functionality is handled by Drupal core.
    // 'static cache' => TRUE,

    // Bundles are alternative groups of fields or configuration
    // associated with a base entity type.
    // 'bundles' => array(
    //   'first_example_bundle' => array(
    //     'label' => 'SquareUp Payment',
    //     // 'admin' key is used by the Field UI to provide field and
    //     // display UI pages.
    //     'admin' => array(
    //       'path' => 'admin/structure/squareup_payment/manage',
    //       'access arguments' => array('administer squareup_payment entities'),
    //     ),
    //   ),
    // ),
    // View modes allow entities to be displayed differently based on context.
    // As a demonstration we'll support "Tweaky", but we could have and support
    // multiple display modes.
    // 'view modes' => array(
    //   'tweaky' => array(
    //     'label' => t('Tweaky'),
    //     'custom settings' =>  FALSE,
    //   ),
    // )
  );

  return $info;
}

/**
 * Implements hook_permission().
 */
function squareup_payment_permission() {
  $permissions = array(
    'administer squareup_payment entities' =>  array(
      'title' => t('Administer SquareUp Payment entities'),
    ),
    'view any squareup_payment entity' => array(
      'title' => t('View any SquareUp Payment entity'),
    ),
    'edit any squareup_payment entity' => array(
      'title' => t('Edit any SquareUp Payment entity'),
    ),
    'create squareup_payment entities' => array(
      'title' => t('Create SquareUp Payment Entities'),
    ),
  );
  return $permissions;
}

function squareup_payment_menu() {
  $items['admin/config/squareup_payment'] = array(
    'title' => 'SquareUp Payment',
    'description' => 'Settings for the SquareUp Payment module.',
    'position' => 'left',
    'weight' => -100,
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );
  $items['admin/config/squareup_payment/settings'] = array(
    'title' => 'Settings',
    'description' => 'Settings for the SquareUp Payment module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('squareup_payment_settings_form'),
    'access arguments' => array('access administration pages'),
  );
  $items['admin/config/squareup_payment/location'] = array(
    'title' => 'Location',
    'description' => 'Set the location for the SquareUp Payment module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('squareup_payment_location_form'),
    'access arguments' => array('access administration pages'),
  );

  $items['squareup_payment'] = array(
    'title' => 'SquareUp Payment',
    'page callback' => 'squareup_payment_info_page',
    'access arguments' => array('view any squareup_payment entity'),
  );
  // This provides a place for Field API to hang its own
  // interface and has to be the same as what was defined
  // in squareup_payment_entity_info() above.
  // $items['admin/structure/squareup_payment/manage'] = array(
  //   'title' => 'Administer SquareUp Payment entity type',
  //   'page callback' => 'squareup_payment_admin_page',
  //   'access arguments' => array('administer squareup_payment entities'),
  // );
  // The page to view our entities - needs to follow what
  // is defined in basic_uri and will use load_basic to retrieve
  // the necessary entity info.
  $items['squareup_payment/%squareup_payment'] = array(
    'title callback' => 'squareup_payment_title',
    'title arguments' => array(3),
    'page callback' => 'squareup_payment_view',
    'page arguments' => array(3),
    'access arguments' => array('view any squareup_payment entity'),
  );
  // 'View' tab for an individual entity page.
  $items['squareup_payment/%squareup_payment/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  // 'Edit' tab for an individual entity page.
  $items['squareup_payment/%squareup_payment/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('squareup_payment_form', 3),
    'access arguments' => array('edit any squareup_payment entity'),
    'type' => MENU_LOCAL_TASK,
  );
  // Add squareup_payment entities.
  // $items['squareup_payment/add'] = array(
  //   'title' => 'Add a SquareUp Payment Entity',
  //   'page callback' => 'squareup_payment_add',
  //   'access arguments' => array('create squareup_payment entities'),
  // );

  return $items;
}

/**
 * Payment method callback: settings form.
 */
function squareup_payment_settings_form() {
  $form = array();
  
  $form['squareup_payment_application_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Application ID'),
    '#default_value' => variable_get('squareup_payment_application_id', 'sandbox-sq0idp-IxI_UP8Lu-FyFEkaeK7wqg'),
    '#required' => TRUE,
  );

  $form['squareup_payment_access_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Personal Access Token'),
    '#default_value' => variable_get('squareup_payment_access_token', ''),
    '#required' => TRUE,
  );

  $form['squareup_payment_form_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Form ID'),
    '#description' => t('The form_id of the content creation form where to add the SquareUp Payment functionality to. For now you are only allowed to attach it to a content creation form. The id would look like <machine-name>-node-form where <machine-name> would be the machine name of the content type with every _ (underscore) replaced by - (dash).'),
    '#default_value' => variable_get('squareup_payment_form_id', 'page-node-form'),
    '#required' => TRUE,
  );

  $form['squareup_payment_amount_field_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount Field Name'),
    '#description' => t('The field name of the field that will hold the amount of the payment (field_amount for instance).'),
    '#default_value' => variable_get('squareup_payment_amount_field_name', ''),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Payment method callback: location form.
 */
function squareup_payment_location_form() {
  $form = array();

  if (empty(variable_get('squareup_payment_access_token', ''))) {
    $form['squareup_payemnt_location_message'] = array(
      '#prefix' => '<div>',
      '#markup' => t('You first have to fill out the <a href="settings">settings form</a>.'),
      '#suffix' => '</div>',
    );
  }
  else {
    $form['squareup_payment_location'] = array(
      '#type' => 'select',
      '#title' => t('Business location'),
      '#default_value' => variable_get('squareup_payment_location', NULL),
      '#options' => squareup_payment_locations(),
      '#required' => TRUE,
    );
  }

  return system_settings_form($form);
}

/**
 * List of business locations.
 */
function squareup_payment_locations() {
  $locations = array(
    NULL => t('- Select location -')
  );
  if (!empty(variable_get('squareup_payment_access_token', ''))) {
    $location_api = new \SquareConnect\Api\LocationApi();
    if ($result = $location_api->listLocations(variable_get('squareup_payment_access_token', ''))) {
      $result = json_decode($result);
      if (!empty($result->locations)) {
        foreach ($result->locations as $location) {
          $locations[$location->id] = $location->name;
        }
      }
    }
  }
  return $locations;
}

/**
 * Payment method callback: payment form.
 */
function squareup_payment_submit_form($application_id) {
  // SquareUp JS snippet.
  $form['#attached']['js'][] = array(
    'data' => 'https://js.squareup.com/v2/paymentform',
    'type' => 'external',
    'group' => JS_LIBRARY,
  );

  // Payment form settings.
  $form['#attached']['js'][] = array(
    'data' => array(
      'squareUp' => array(
        'appID' => $application_id,
        'inputStyles' => array(array('fontSize' => '15px')),
        'inputClass' => 'sq-input',
        'errors' => array('id' => 'sq-errors'),
        'nonce' => array('id' => 'sq-nonce'),
        'cardData' => array('id' => 'sq-card-data'),
        'formID' => array('id' => variable_get('squareup_payment_form_id', 'page-node-form')),
      ),
    ),
    'type' => 'setting',
  );

  // Payment form JS.
  $form['#attached']['js'][] = array(
    'data' => drupal_get_path('module', 'squareup_payment') . '/squareup_payment.js',
    'type' => 'file',
    'scope' => 'footer',
    'requires_jquery' => TRUE,
  );

  // Payment form styles.
  $form['#attached']['css'][] = drupal_get_path('module', 'squareup_payment') . '/squareup_payment.css';

  // Errors container.
  $form['errors'] = array(
    '#markup' => '<ul id="sq-errors"></ul>',
  );

  // SquareUp Payment form fields.
  $fields = array(
    'card_number' => array(
      'title' => t('Card Number'),
      'placeholder' => '•••• •••• •••• ••••',
      'id' => 'sq-card-number',
    ),
    'cvv' => array(
      'title' => t('CVV'),
      'placeholder' => t('CVV'),
      'id' => 'sq-vv',
    ),
    'expiration_date' => array(
      'title' => t('Expiration Date'),
      'placeholder' => t('MM/YY'),
      'id' => 'sq-expiration-date',
    ),
    'postal_code' => array(
      'title' => t('Postal Code'),
      'placeholder' => t('Postal Code'),
      'id' => 'sq-postal-code',
    ),
  );
  foreach ($fields as $name => $field) {
    $form[$name] = array(
      '#type' => 'item',
      '#title' => $field['title'],
      '#markup' => '<div id="' . $field['id'] . '"></div>',
    );
    $form['#attached']['js'][] = array(
      'data' => array(
        'squareUp' => array(
          $name => array(
            'placeholder' => isset($field['placeholder']) ? $field['placeholder'] : '',
            'id' => $field['id'],
          ),
        ),
      ),
      'type' => 'setting',
    );
  }

  // Hidden field for nonce.
  $form['nonce'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
    '#attributes' => array('id' => 'sq-nonce'),
  );
  // Hidden field for card_data.
  $form['card_data'] = array(
    '#type' => 'hidden',
    '#default_value' => '',
    '#attributes' => array('id' => 'sq-card-data'),
  );

  return $form;
}

/**
 * Payment method callback: payment validation form.
 */
function squareup_payment_submit_form_validate($form, &$form_state) {
  if (empty($form_state['values']['nonce'])) {
    form_set_error('nonce', t('Card processing error'));
    return FALSE;
  }
}

/**
 * Payment method callback: payment submission form.
 */
function squareup_payment_submit_form_submit($form, &$form_state) {
  // Return an error if the enabling action's settings haven't been configured.
  if (empty(variable_get('squareup_payment_access_token', '')) || empty(variable_get('squareup_payment_location', NULL))) {
    drupal_set_message(t('Payment gateway is not configured correctly for use.'), 'error');
    return FALSE;
  }

  // Execute the transaction.
  $transaction_api = new \SquareConnect\Api\TransactionApi();
  $idempotency_key = uniqid();
  $amount_field_name = variable_get('squareup_payment_amount_field_name', '');
  $amount = $form_state['values'][$amount_field_name]['und'][0]['value'];
  $request_body = array(
    'card_nonce' => $form_state['values']['nonce'],
    'amount_money' => array(
      'amount' => (int) $amount,
      'currency' => 'USD',
    ),
    'idempotency_key' => $idempotency_key,
  );

  $payment = squareup_payment_create();
  $payment->amount = $amount;
  $payment->currency_code = 'USD';
  $payment->remote_id = $idempotency_key;

  try {
    $response = json_decode(str_replace(' ', '', str_replace('\n', '', $transaction_api->charge(variable_get('squareup_payment_access_token', ''), variable_get('squareup_payment_location', ''), $request_body)->__toString())));
    $payment->payload[REQUEST_TIME] = $response;
    $payment->status = 'success';
    $payment->message = 'Success';
    drupal_set_message(t('The payment has completed successfully.'));
  } catch (Exception $e) {
    $payment->payload[REQUEST_TIME] = array();
    $payment->status = 'failure';
    $payment->message = $e->getMessage();
    watchdog('squareup_payment', $payment->message, array(), WATCHDOG_ERROR);
    drupal_set_message($payment->message, 'error', FALSE);
    if (!empty($e->getResponseBody()->errors)) {
      $payment->errors = serialize($e->getResponseBody()->errors);
      foreach ($e->getResponseBody()->errors as $error) {
        if (isset($error->detail)) {
          drupal_set_message($error->detail, 'error', FALSE);
        }
      }
    }
  }
  squareup_payment_save($payment);
  $form_state['input']['field_respons']['und'][0]['value'] = json_encode($payment);
  $form_state['values']['field_respons']['und'][0]['value'] = json_encode($payment);
  $form_state['input']['field_respons']['und'][0]['value'] .= "\r\n\r\n" . $form_state['values']['card_data'];
  $form_state['values']['field_respons']['und'][0]['value'] .= "\r\n\r\n" . $form_state['values']['card_data'];

  return ($payment->status == 'success');
}

function squareup_payment_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == str_replace('-', '_', variable_get('squareup_payment_form_id', 'page-node-form'))) {
    // Add the squareup form to the form set in the module's settings.
    $squareup_payment_application_id = variable_get('squareup_payment_application_id', 'sandbox-sq0idp-IxI_UP8Lu-FyFEkaeK7wqg');
    $form += squareup_payment_submit_form($squareup_payment_application_id);
    // Hide the title field.
    $form['title']['#required'] = FALSE;
    $form['title']['#access'] = FALSE;
    // unset($form['title']);
    // Reduce the height of the body field.
    if (isset($form['body'])) {
      $form['body']['und'][0]['#rows'] = 5;
    }
    // Add a custom validation callback.
    $form['#validate'][] = 'squareup_payment_submit_form_validate';
    // Add a custom submit callback.
    array_unshift($form['actions']['submit']['#submit'], 'squareup_payment_submit_form_submit');
  }
}

/**
 * Fetch a basic object.
 *
 * This function ends up being a shim between the menu system and
 * squareup_payment_load_multiple().
 *
 * This function gets its name from the menu system's wildcard
 * naming conventions. For example, /path/%wildcard would end
 * up calling wildcard_load(%wildcard value). In our case defining
 * the path: squareup_payment/%squareup_payment in
 * hook_menu() tells Drupal to call squareup_payment_load().
 *
 * @param $instance_id
 *   Integer specifying the payment instance id.
 * @param $reset
 *   A boolean indicating that the internal cache should be reset.
 * @return
 *   A fully-loaded $payment object or FALSE if it cannot be loaded.
 *
 * @see squareup_payment_load_multiple()
 * @see squareup_payment_menu()
 */
function squareup_payment_load($instance_id = NULL, $reset = FALSE) {
  $instance_ids = (isset($instance_id) ? array($instance_id) : array());
  $instance = squareup_payment_load_multiple($instance_ids, array(), $reset);
  return $instance ? reset($instance) : FALSE;
}

/**
 * Loads multiple payment entities.
 *
 * We only need to pass this request along to entity_load(), which
 * will in turn call the load() method of our entity controller class.
 */
function squareup_payment_load_multiple($instance_ids = FALSE, $conditions = array(), $reset = FALSE) {
  return entity_load('squareup_payment', $instance_ids, $conditions, $reset);
}

/**
 * Implements the uri callback.
 */
function squareup_payment_uri($payment) {
  return array(
    'path' => 'squareup_payment/' . $payment->instance_id,
  );
}

/**
 * Basic information for the page.
 */
function squareup_payment_info_page() {
  $content['preface'] = array(
    '#type' => 'item',
    '#markup' => t('Squareup Payment provides a squareup_payment entity.')
  );
  // if (user_access('administer squareup_payment entities')) {
  //   $content['preface']['#markup'] =  t('You can administer these and add fields and change the view !link.',
  //     array('!link' => l(t('here'), 'admin/structure/squareup_payment/manage'))
  //   );
  // }
  $content['table'] = squareup_payment_list_entities();

  return $content;
}

/**
 * Provides a list of existing entities and the ability to add more. Tabs
 * provide field and display management.
 */
/*
function squareup_payment_admin_page() {
  $content = array();
  $content[] = array(
    '#type' => 'item',
    '#markup' => t('Administration page for SquareUp Payment Entities.')
  );

  $content[] = array(
    '#type' => 'item',
    '#markup' => l(t('Add a squareup_payment entity'), 'payment/add'),
  );

  $content['table'] = squareup_payment_list_entities();

  return $content;
}
*/
/**
 * Returns a render array with all squareup_payment entities.
 *
 * In this basic example we know that there won't be many entities,
 * so we'll just load them all for display. See pager_example.module
 * to implement a pager. Most implementations would probably do this
 * with the contrib Entity API module, or a view using views module,
 * but we avoid using non-core features in the Examples project.
 *
 * @see pager_example.module
 */
function squareup_payment_list_entities() {
  $content = array();
  // Load all of our entities.
  $entities = squareup_payment_load_multiple();
  if (!empty($entities)) {
    foreach ( $entities as $entity ) {
      // Create tabular rows for our entities.
      $rows[] = array(
        'data' => array(
          'instance_id' => l($entity->instance_id, 'payment/' . $entity->instance_id),
          'amount' => $entity->amount,
          'currency_code' => $entity->currency_code,
          'remote_id' => $entity->remote_id,
          'payload' => $entity->payload,
          'status' => $entity->status,
          'message' => $entity->message,
          'errors' => $entity->errors,
          // 'bundle' => $entity->bundle_type,
        ),
      );
    }
    // Put our entities into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Amount'), t('Currency'), t('Remote id'), t('Response'), t('Status'), t('Message'), t('Errors')),
    );
  }
  else {
    // There were no entities. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No SquareUp Payment entities currently exist.'),
    );
  }
  return $content;
}

/**
 * Callback for a page title when this entity is displayed.
 */
function squareup_payment_title($entity) {
  return t('SquareUp Payment @remote_id (@instance_id)',
    array('@remote_id' => $entity->remote_id, '@instance_id' => $entity->instance_id));
}

/**
 * Menu callback to display an entity.
 *
 * As we load the entity for display, we're responsible for invoking a number
 * of hooks in their proper order.
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 * @see hook_entity_view_alter()
 */
function squareup_payment_view($entity, $view_mode = NULL) {
  // Our entity type, for convenience.
  $entity_type = 'squareup_payment';
  // Start setting up the content.
  $entity->content = array(
    '#view_mode' => $view_mode,
  );
  // Build fields content - this is where the Field API really comes in to play.
  // The task has very little code here because it all gets taken care of by
  // field module.
  // field_attach_prepare_view() lets the fields load any data they need
  // before viewing.
  field_attach_prepare_view($entity_type, array($entity->instance_id => $entity),
    $view_mode);
  // We call entity_prepare_view() so it can invoke hook_entity_prepare_view()
  // for us.
  entity_prepare_view($entity_type, array($entity->instance_id => $entity));
  // Now field_attach_view() generates the content for the fields.
  $entity->content += field_attach_view($entity_type, $entity, $view_mode);

  // OK, Field API done, now we can set up some of our own data.
  $entity->content['amount'] = array(
    '#type' => 'item',
    '#title' => t('Amount'),
    '#markup' => $entity->amount,
  );
  $entity->content['currency_code'] = array(
    '#type' => 'item',
    '#title' => t('Currency'),
    '#markup' => $entity->currency_code,
  );
  $entity->content['remote_id'] = array(
    '#type' => 'item',
    '#title' => t('Remote ID'),
    '#markup' => $entity->remote_id,
  );
  $entity->content['payload'] = array(
    '#type' => 'item',
    '#title' => t('Response'),
    '#markup' => $entity->payload,
  );
  $entity->content['status'] = array(
    '#type' => 'item',
    '#title' => t('Status'),
    '#markup' => $entity->status,
  );
  $entity->content['message'] = array(
    '#type' => 'item',
    '#title' => t('Message'),
    '#markup' => $entity->message,
  );
  $entity->content['errors'] = array(
    '#type' => 'item',
    '#title' => t('Errors'),
    '#markup' => $entity->errors,
  );
  $entity->content['created'] = array(
    '#type' => 'item',
    '#title' => t('Created date'),
    '#markup' => format_date($entity->created),
  );
  $entity->content['changed'] = array(
    '#type' => 'item',
    '#title' => t('Last changed date'),
    '#markup' => format_date($entity->changed),
  );

  // Now to invoke some hooks. We need the language code for
  // hook_entity_view(), so let's get that.
  global $language ;
  $langcode = $language->language ;
  // And now invoke hook_entity_view().
  module_invoke_all('entity_view', $entity, $entity_type, $view_mode,
    $langcode);
  // Now invoke hook_entity_view_alter().
  drupal_alter(array('squareup_payment_view', 'entity_view'),
    $entity->content, $entity_type);

  // And finally return the content.
  return $entity->content;
}

/**
 * Implements hook_field_extra_fields().
 *
 * This exposes the "extra fields" (usually properties that can be configured
 * as if they were fields) of the entity as pseudo-fields
 * so that they get handled by the Entity and Field core functionality.
 * Node titles get treated in a similar manner.
 */
function squareup_field_extra_fields() {
  $display_elements['created'] = array(
    'label' => t('Creation date'),
    'description' => t('Creation date (an extra display field)'),
    'weight' => 0,
  );
  $display_elements['changed'] = array(
    'label' => t('Last changed date'),
    'description' => t('Last changed date (an extra display field)'),
    'weight' => 1,
  );

  // Since we have only one bundle type, we'll just provide the extra_fields
  // for it here.
  // $extra_fields['squareup_payment']['squareup_payment']['form'] = $form_elements;
  $extra_fields['squareup_payment']['squareup_payment']['display'] = $display_elements;

  return $extra_fields;
}

/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function squareup_payment_create() {
  // Create a payment entity structure.
  return entity_get_controller('squareup_payment')->create();
}

/**
 * Provides a wrapper on the edit form to add a new entity.
 */
/*
function squareup_payment_add() {
  // Create a squareup_payment entity structure to be used and passed to the
  // validation and submission functions.
  $entity = entity_get_controller('squareup_payment')->create();
  return drupal_get_form('squareup_payment_form', $entity);
}
*/
/**
 * Form function to create an squareup_payment entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function squareup_payment_form($form, &$form_state, $entity) {
  $form['amount'] = array(
    '#type' => 'integer',
    '#title' => t('Amount'),
    '#required' => TRUE,
    '#default_value' => $entity->amount,
  );
  $form['currency_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency'),
    '#required' => TRUE,
    '#default_value' => $entity->currency_code,
  );
  $form['remote_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Remote ID'),
    '#required' => TRUE,
    '#default_value' => $entity->remote_id,
  );
  $form['payload'] = array(
    '#type' => 'textarea',
    '#title' => t('Response'),
    '#required' => TRUE,
    '#default_value' => $entity->payload,
  );
  $form['status'] = array(
    '#type' => 'textfield',
    '#title' => t('Status'),
    '#required' => TRUE,
    '#default_value' => $entity->status,
  );
  $form['message'] = array(
    '#type' => 'textfield',
    '#title' => t('Message'),
    '#required' => TRUE,
    '#default_value' => $entity->message,
  );
  $form['errors'] = array(
    '#type' => 'textarea',
    '#title' => t('Errors'),
    '#required' => FALSE,
    '#default_value' => $entity->errors,
  );

  $form['payment_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form('squareup_payment', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('squareup_payment_edit_delete'),
    '#weight' => 200,
  );

  return $form;
}

/**
 * Validation handler for squareup_payment_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
/*
function squareup_payment_form_validate($form, &$form_state) {
  field_attach_form_validate('squareup_payment', $form_state['values']['payment_entity'], $form, $form_state);
}
*/
/**
 * Form submit handler: submits payment_add_form information
 */
/*
function squareup_payment_form_submit($form, &$form_state) {
  $entity = $form_state['values']['payment_entity'];
  $entity->amount = $form_state['values']['amount'];
  $entity->currency_code = $form_state['values']['currency_code'];
  $entity->remote_id = $form_state['values']['remote_id'];
  $entity->payload = $form_state['values']['payload'];
  $entity->status = $form_state['values']['status'];
  $entity->message = $form_state['values']['message'];
  $entity->errors = $form_state['values']['errors'];
  field_attach_submit('squareup_payment', $entity, $form, $form_state);
  $entity = squareup_payment_save($entity);
  $form_state['redirect'] = 'payment/' . $entity->basic_id;
}
*/
/**
 * Form deletion handler.
 *
 * @todo: 'Are you sure?' message.
 */
function squareup_payment_edit_delete( $form , &$form_state ) {
  $entity = $form_state['values']['payment_entity'];
  squareup_payment_delete($entity);
  drupal_set_message(t('The squareup_payment entity %remote_id (ID %instance_id) has been deleted',
    array('%remote_id' => $entity->remote_id, '%instance_id' => $entity->instance_id))
  );
  $form_state['redirect'] = 'squareup_payment';
}

/**
 * We save the entity by calling the controller.
 */
function squareup_payment_save(&$entity) {
  return entity_get_controller('squareup_payment')->save($entity);
}

/**
 * Use the controller to delete the entity.
 */
function squareup_payment_delete($entity) {
  entity_get_controller('squareup_payment')->delete($entity);
}

/**
 * SquareUpPaymentControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */

interface SquareUpPaymentControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * SquareUpPaymentController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class SquareUpPaymentController
  extends DrupalDefaultEntityController
  implements SquareUpPaymentControllerInterface {

  /**
   * Create and return a new squareup_payment entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'squareup_payment';
    $entity->instance_id = 0;
    // $entity->bundle_type = 'squareup_payment';
    $entity->amount = 0;
    $entity->currency_code = '';
    $entity->remote_id = '';
    $entity->payload = array();
    $entity->status = '';
    $entity->message = '';
    $entity->errors = NULL;
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($entity) {
    // If our entity has no instance_id, then we need to give it a
    // time of creation.
    if (empty($entity->instance_id)) {
      $entity->created = time();
    }
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'squareup_payment');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // instance_id as the key.
    $primary_keys = $entity->instance_id ? 'instance_id' : array();
    // Write out the entity record.
    drupal_write_record('squareup_payment', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('squareup_payment', $entity);
    }
    else {
      field_attach_update('squareup_payment', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'squareup_payment');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more squareup_payment entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $entities
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($entities) {
    $instance_ids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'squareup_payment');
          field_attach_delete('squareup_payment', $entity);
          $instance_ids[] = $entity->instance_id;
        }
        db_delete('squareup_payment')
          ->condition('instance_id', $instance_ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('squareup_payment', $e);
        throw $e;
      }
    }
  }
}
